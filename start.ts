import { HuobiRestAPI } from ".";
import { config } from "./config";

const api = new HuobiRestAPI({
    secretKey: config.keys.api_secret,
    accessKey: config.keys.api_key,
    hostname: 'api.hbdm.com'
})

//example
api.post('/api/v1/contract_order', {
    contract_code: 'fil210924',
    volume: '1',
    offset: 'open',
    lever_rate: '5',
    direction: 'sell',
    order_price_type: 'limit',
    price: 200
}).catch(err => {
    console.log(err)
})